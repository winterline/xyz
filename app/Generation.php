<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Generation extends Model
{
    protected $fillable = ['name', 'description', 'slug', 'image'];

    public function tests()
    {
        return $this->belongsToMany('App\Test');
    }

    public function masters()
    {
        return $this->belongsToMany('App\Master');
    }

    public function olympiads()
    {
        return $this->belongsToMany('App\Olympiad');
    }

    public function projects()
    {
        return $this->belongsToMany('App\Project');
    }

    public function links()
    {
        return $this->belongsToMany('App\Link');
    }
}
