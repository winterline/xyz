<?php

namespace App\Http\Controllers;

use App\About;
use App\Generation;
use Illuminate\Http\Request;

class AboutController extends Controller
{
    // ABOUT

    public function __construct()
    {
        $this->middleware('can:admin-panel');
    }

    public function about()
    {
        $about = About::all()->first();
        $generations = Generation::all();
        return view('admin.about.index', compact(['about', 'generations']));
    }

    public function update_about(Request $request)
    {
        $about = About::all()->first();

        $about->description = $request->get('description');
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $fileName = $file->getClientOriginalName();
            $file->move(public_path() . '/image', $fileName);
        } else {
            $fileName = $about['image'];
        }

        $about->image = $fileName;
        $about->save();
        return back();
    }
}
