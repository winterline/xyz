<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Generation;
use App\News;
use App\Contact;
use App\User;
use App\UserPay;


class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:admin-panel');
    }

    public function admin()
    {
        $generations = Generation::all();
        $news = News::all();
        return view('admin', compact(['generations', 'news']));
    }

    // CONTACTS
    public function contacts()
    {
        $contacts = Contact::all()->first();
        $generations = Generation::all();
        return view('admin.contacts.index', compact(['contacts', 'generations']));
    }

    public function update_contacts(Request $request)
    {
        $contacts = Contact::all()->first();

        $contacts->phone = $request->get('phone');
        $contacts->email = $request->get('email');
        $contacts->address = $request->get('address');
        $contacts->time_work = $request->get('time_work');

        $contacts->save();
        return back();
    }

    // USERS
    public function users()
    {
        $usersPay = UserPay::all()->reverse();
        $users = User::all();
        $generations = Generation::all();
        return view('admin.users.index', compact(['users', 'generations', 'usersPay']));
    }

    // GENERATIONS
    public function generations($slug)
    {

        $generations = Generation::all();
        $generation = Generation::where('slug', $slug)->first();
        $id = $generation->id;
        $tests = Generation::find($id)->tests;
        $olympiads = Generation::find($id)->olympiads;
        $masters = Generation::find($id)->masters;
        $projects = Generation::find($id)->projects;
        $links = Generation::find($id)->links;

        return view('admin.generations.index', compact([
            'generation',
            'tests',
            'generations',
            'masters',
            'olympiads',
            'projects',
            'links'
        ]));
    }

    public function update_generations(Request $request, $id)
    {
        $generation = Generation::find($id);

        $generation->name = $request->get('name');
        $generation->description = $request->get('description');
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $fileName = $file->getClientOriginalName();
            $file->move(public_path() . '/image', $fileName);
        } else {
            $fileName = $generation['image'];
        }

        $generation->image = $fileName;
        $generation->save();
        return back();
    }
}
