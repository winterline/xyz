<?php

namespace App\Http\Controllers;

use App\Link;
use App\Generation;
use Illuminate\Http\Request;

class LinkController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:admin-panel');
    }

    // LINKS
    public function store_link(Request $request)
    {
        $id_generation = $request->get('id_generation');
        $data = $request->all();
        unset($data['id_generation']);
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $fileName = $file->getClientOriginalName();
            $file->move(public_path() . '/image', $fileName);
        }

        $data['image'] = $fileName;
        $data = Link::create($data);

        $data->generations()->save(Generation::find($id_generation));

        return back();
    }

    public function edit_link($id)
    {
        $generations = Generation::all();
        $link = Link::find($id);

        return view('admin.link.edit', compact(['generations', 'link']));
    }

    public function update_link(Request $request, $id)
    {
        $data = Link::find($id);

        $data->title = $request->get('title');
        $data->description = $request->get('description');
        $data->link = $request->get('link');

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $fileName = $file->getClientOriginalName();
            $file->move(public_path() . '/image', $fileName);
        } else {
            $fileName = $data['image'];
        }

        $data->image = $fileName;

        $data->save();
        return redirect('admin/index');
    }

    public function delete_link($id)
    {
        $data = Link::find($id);
        $data->delete();
        return back();
    }
}
