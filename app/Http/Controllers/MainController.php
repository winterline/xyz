<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Generation;
use App\News;
use App\About;
use App\Contact;
use App\Test;
use App\Master;
use App\Olympiad;
use App\Project;
use App\Service;
use App\User;

class MainController extends Controller
{
    public function index()
    {
        $generations = Generation::all();
        $news = News::all()->reverse();
        $about = About::all();
        $services = Service::all()->reverse();
        $contacts = Contact::all();

        $sliders = $news->take(4);

        return view('welcome', compact(['generations', 'news', 'about', 'contacts', 'services', 'sliders']));
    }

    public function generations($slug)
    {
        $generations = Generation::all();
        $generation = Generation::where('slug', $slug)->first();
        $id = $generation->id;
        $tests = Generation::find($id)->tests;
        $olympiads = Generation::find($id)->olympiads;
        $masters = Generation::find($id)->masters;
        $projects = Generation::find($id)->projects;
        $links = Generation::find($id)->links;
        $contacts = Contact::all()->first();

        return view('generation', compact([
            'generation',
            'tests',
            'generations',
            'masters',
            'olympiads',
            'projects',
            'links',
            'contacts'
        ]));
    }
    public function example($slug, $item, $id)
    {
        $generation = Generation::where('slug', $slug)->first();
        if ($item == 'tests') {
            $items = Test::find($id);
        }
        if ($item == 'masters') {
            $items = Master::find($id);
        }
        if ($item == 'olympiads') {
            $items = Olympiad::find($id);
        }
        if ($item == 'projects') {
            $items = Project::find($id);
        }
        $contacts = Contact::all()->first();
        return view('item', compact(['contacts', 'items']));
    }

    public function search()
    {
        $contacts = Contact::all()->first();
        return view('search', compact(['contacts']));
    }
}
