<?php

namespace App\Http\Controllers;

use App\Master;
use App\Generation;
use Illuminate\Http\Request;

class MasterController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:admin-panel');
    }
    // MASTERS
    public function store_master(Request $request)
    {
        $id_generation = $request->get('id_generation');
        $data = $request->all();
        unset($data['id_generation']);
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $fileName = $file->getClientOriginalName();
            $file->move(public_path() . '/file', $fileName);
        }

        if ($request->hasFile('pre_file')) {
            $pre_file = $request->file('pre_file');
            $pre_fileName = $pre_file->getClientOriginalName();
            $pre_file->move(public_path() . '/file', $pre_fileName);
        }


        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $imageName = $file->getClientOriginalName();
            $file->move(public_path() . '/image', $imageName);
        }



        $data['file'] = $fileName;
        $data['pre_file'] = $pre_fileName;
        $data['image'] = $imageName;

        $data = Master::create($data);

        $data->generations()->save(Generation::find($id_generation));

        return back();
    }

    public function edit_master($id)
    {
        $generations = Generation::all();
        $master = Master::find($id);

        return view('admin.master.edit', compact(['generations', 'master']));
    }

    public function update_master(Request $request, $id)
    {
        $data = Master::find($id);

        $data->title = $request->get('title');
        $data->description = $request->get('description');

        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $fileName = $file->getClientOriginalName();
            $file->move(public_path() . '/file', $fileName);
        } else {
            $fileName = $data['file'];
        }

        if ($request->hasFile('pre_file')) {
            $pre_file = $request->file('pre_file');
            $pre_fileName = $pre_file->getClientOriginalName();
            $pre_file->move(public_path() . '/file', $pre_fileName);
        } else {
            $pre_fileName = $data['pre_file'];
        }

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = $image->getClientOriginalName();
            $image->move(public_path() . '/file', $imageName);
        } else {
            $imageName = $data['image'];
        }

        $data->file = $fileName;
        $data->pre_file = $pre_fileName;
        $data->image = $imageName;

        $data->save();
        return redirect('admin/index');
    }

    public function delete_master($id)
    {
        $data = Master::find($id);
        $data->delete();
        return back();
    }
}
