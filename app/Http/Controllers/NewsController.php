<?php

namespace App\Http\Controllers;

use App\News;
use App\Generation;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:admin-panel');
    }
    // NEWS
    public function news()
    {
        $news = News::all()->reverse();
        $generations = Generation::all();

        return view('admin.news.news', compact('news', 'generations'));
    }

    public function create_news()
    {
        $generations = Generation::all();
        return view('admin.news.create_news', compact('generations'));
    }

    public function store_news(Request $request)
    {

        $data = $request->all();
        // dd($data);
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $fileName = $file->getClientOriginalName();
            $file->move(public_path() . '/image', $fileName);
        }

        $data['image'] = $fileName;
        $data = News::create($data);

        return redirect('admin/news');
    }

    public function edit_news($id)
    {
        $news = News::find($id);
        $generations = Generation::all();

        return view('admin.news.edit_news', compact('news', 'generations'));
    }

    public function update_news(Request $request, $id)
    {
        $news = News::find($id);

        $news->title = $request->get('title');
        $news->description = $request->get('description');

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $fileName = $file->getClientOriginalName();
            $file->move(public_path() . '/image', $fileName);
        } else {
            $fileName = $news['image'];
        }

        $news->image = $fileName;

        $news->save();
        return redirect('admin/news');
    }

    public function delete_news($id)
    {
        $news = News::find($id);
        $news->delete();
        return back();
    }
}
