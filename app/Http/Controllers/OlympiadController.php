<?php

namespace App\Http\Controllers;

use App\Olympiad;
use App\Generation;
use Illuminate\Http\Request;

class OlympiadController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:admin-panel');
    }
    // OLYMPIAD
    public function store_olympiad(Request $request)
    {
        $id_generation = $request->get('id_generation');

        $data = $request->all();
        unset($data['id_generation']);
        // dd($data);
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $fileName = $file->getClientOriginalName();
            $file->move(public_path() . '/file', $fileName);
        }

        if ($request->hasFile('pre_file')) {
            $pre_file = $request->file('pre_file');
            $pre_fileName = $pre_file->getClientOriginalName();
            $pre_file->move(public_path() . '/file', $pre_fileName);
        }


        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $imageName = $file->getClientOriginalName();
            $file->move(public_path() . '/image', $imageName);
        }



        $data['file'] = $fileName;
        $data['pre_file'] = $pre_fileName;
        $data['image'] = $imageName;
        $data = Olympiad::create($data);

        $data->generations()->save(Generation::find($id_generation));

        return back();
    }

    public function edit_olympiad($id)
    {
        $generations = Generation::all();
        $olympiad = Olympiad::find($id);

        return view('admin.olympiad.edit', compact(['generations', 'olympiad']));
    }

    public function update_olympiad(Request $request, $id)
    {
        $data = Olympiad::find($id);

        $data->title = $request->get('title');
        $data->description = $request->get('description');

        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $fileName = $file->getClientOriginalName();
            $file->move(public_path() . '/file', $fileName);
        } else {
            $fileName = $data['file'];
        }

        if ($request->hasFile('pre_file')) {
            $pre_file = $request->file('pre_file');
            $pre_fileName = $pre_file->getClientOriginalName();
            $pre_file->move(public_path() . '/file', $pre_fileName);
        } else {
            $pre_fileName = $data['pre_file'];
        }

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = $image->getClientOriginalName();
            $image->move(public_path() . '/file', $imageName);
        } else {
            $imageName = $data['image'];
        }

        $data->file = $fileName;
        $data->pre_file = $pre_fileName;
        $data->image = $imageName;

        $data->save();
        return redirect('admin/index');
    }

    public function delete_olympiad($id)
    {
        $data = Olympiad::find($id);
        $data->delete();
        return back();
    }
}
