<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserPay;

class SendEmailController extends Controller
{
    public function sendFileContest(Request $request)
    {
        $id = $request->get('id');

        $userPay = UserPay::find($id);
        $email = $userPay->user_email;

        $result = \Mail::send('email.template_send_file', ['data' => $userPay], function ($message) use ($userPay) {

            $mail_admin = env('MAIL_USERNAME', 'null@mail.ru');
            $message->from($mail_admin, 'Сайт ПОРА');
            $message->to($userPay->user_email)->subject('Контест с сайта ПОРА');;
        });

        return back()->with('message', 'Вы успешно отправили контест.');
    }

    public function callback(Request $request)
    {
        $data = $request->all();
        // dd($data);

        $result = \Mail::send('email.callback', ['data' => $data], function ($message) use ($data) {
            $mail_admin = env('MAIL_USERNAME', 'null@mail.ru');
            $message->from($mail_admin, 'Сайт ПОРА');
            $message->to($mail_admin)->subject('Обратная связь');
        });
        return back()->with('message', 'Вы успешно отправили контест.');
    }
}
