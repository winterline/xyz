<?php

namespace App\Http\Controllers;

use App\Service;
use App\Generation;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:admin-panel');
    }
    // SERVICES
    public function services()
    {
        $services = Service::all()->reverse();
        $generations = Generation::all();

        return view('admin.services.index', compact('services', 'generations'));
    }

    public function create_service()
    {
        $generations = Generation::all();
        return view('admin.services.create_service', compact('generations'));
    }

    public function store_service(Request $request)
    {

        $data = $request->all();
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $fileName = $file->getClientOriginalName();
            $file->move(public_path() . '/image', $fileName);
        }

        $data['image'] = $fileName;
        $data = Service::create($data);

        return redirect('admin/services');
    }

    public function edit_service($id)
    {
        $service = Service::find($id);
        $generations = Generation::all();

        return view('admin.services.edit_service', compact('service', 'generations'));
    }

    public function update_service(Request $request, $id)
    {
        $service = Service::find($id);

        $service->title = $request->get('title');
        $service->description = $request->get('description');

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $fileName = $file->getClientOriginalName();
            $file->move(public_path() . '/image', $fileName);
        } else {
            $fileName = $service['image'];
        }

        $service->image = $fileName;

        $service->save();
        return redirect('admin/services');
    }

    public function delete_service($id)
    {
        $service = Service::find($id);
        $service->delete();
        return back();
    }
}
