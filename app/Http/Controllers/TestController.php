<?php

namespace App\Http\Controllers;

use App\Test;
use App\Generation;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:admin-panel');
    }
    // TESTS
    public function store_test(Request $request)
    {

        $id_generation = $request->get('id_generation');

        $data = $request->all();
        unset($data['id_generation']);
        // dd($data);
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $fileName = $file->getClientOriginalName();
            $file->move(public_path() . '/file', $fileName);
        }

        if ($request->hasFile('pre_file')) {
            $pre_file = $request->file('pre_file');
            $pre_fileName = $pre_file->getClientOriginalName();
            $pre_file->move(public_path() . '/file', $pre_fileName);
        }


        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $imageName = $file->getClientOriginalName();
            $file->move(public_path() . '/image', $imageName);
        }



        $data['file'] = $fileName;
        $data['pre_file'] = $pre_fileName;
        $data['image'] = $imageName;
        $data = Test::create($data);

        $data->generations()->save(Generation::find($id_generation));

        return back();
    }

    public function edit_test($id)
    {
        $generations = Generation::all();
        $test = Test::find($id);

        return view('admin.test.edit', compact(['generations', 'test']));
    }

    public function update_test(Request $request, $id)
    {
        $test = Test::find($id);

        $test->title = $request->get('title');
        $test->description = $request->get('description');

        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $fileName = $file->getClientOriginalName();
            $file->move(public_path() . '/file', $fileName);
        } else {
            $fileName = $test['file'];
        }

        if ($request->hasFile('pre_file')) {
            $pre_file = $request->file('pre_file');
            $pre_fileName = $pre_file->getClientOriginalName();
            $pre_file->move(public_path() . '/file', $pre_fileName);
        } else {
            $pre_fileName = $test['pre_file'];
        }

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = $image->getClientOriginalName();
            $image->move(public_path() . '/file', $imageName);
        } else {
            $imageName = $test['image'];
        }

        $test->file = $fileName;
        $test->pre_file = $pre_fileName;
        $test->image = $imageName;

        $test->save();
        return redirect('admin/index');
    }

    public function delete_test($id)
    {
        $data = Test::find($id);
        $data->delete();
        return back();
    }
}
