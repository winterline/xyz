<?php

namespace App\Http\Controllers;

use App\UserPay;
use Illuminate\Http\Request;

class UserPayController extends Controller
{


    public function store(Request $request)
    {
        $data = $request->all();
        $data = UserPay::create($data);
        return back()->with('message', 'Ваша заявка на оплату отправлена! В течении 24 часов, оплаченный контест придет к вам на почту.');
    }
}
