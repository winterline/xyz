<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    protected $fillable = ['title', 'description', 'image', 'link'];

    public function generations()
    {
        return $this->belongsToMany('App\Generation');
    }
}
