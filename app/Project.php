<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = ['title', 'description', 'file', 'sub_title', 'image', 'pre_file'];


    public function generations()
    {
        return $this->belongsToMany('App\Generation');
    }
}
