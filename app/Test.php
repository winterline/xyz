<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Generation;

class Test extends Model
{

    protected $fillable = ['title', 'description', 'file', 'sub_title', 'image', 'pre_file'];


    public function generations()
    {
        return $this->belongsToMany('App\Generation');
    }
}
