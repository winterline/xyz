<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPay extends Model
{
    protected $fillable = ['user_id', 'user_name', 'user_email', 'name_content', 'name_file'];
}
