<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGenerationOlympiadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('generation_olympiad', function (Blueprint $table) {
            $table->bigInteger('generation_id')->unsigned();
            $table->bigInteger('olympiad_id')->unsigned();
            $table->foreign('generation_id')->references('id')->on('generations')->onDelete('cascade');
            $table->foreign('olympiad_id')->references('id')->on('olympiads')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('generation_olympiad');
    }
}
