<?php

use Illuminate\Database\Seeder;

class AboutSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = File::get('database/data/about.json');

        $about = json_decode($path);

        foreach ($about as $item) {
            DB::table('abouts')->insert([
                'description' => $item->description,
                'image' => $item->image,
            ]);
        }
    }
}
