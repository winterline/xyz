<?php

use Illuminate\Database\Seeder;

class ContactsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = File::get('database/data/contacts.json');

        $contacts = json_decode($path);

        foreach ($contacts as $item) {
            DB::table('contacts')->insert([
                'phone' => $item->phone,
                'email' => $item->email,
                'address' => $item->address,
                'time_work' => $item->time_work,
            ]);
        }
    }
}
