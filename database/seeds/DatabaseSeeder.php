<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        $this->call(NewsSeeder::class);
        $this->call(AboutSeeder::class);
        $this->call(ContactsSeeder::class);
        $this->call(GenerationsSeeder::class);
        $this->call(ServicesSeeder::class);
        $this->call(TestsSeeder::class);
        $this->call(LinksSeeder::class);
        $this->call(TemplateSeeder::class);
        $this->call(GenerationsTestsSeeder::class);
    }
}
