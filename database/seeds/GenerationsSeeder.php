<?php

use Illuminate\Database\Seeder;

class GenerationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = File::get('database/data/generations.json');

        $generations = json_decode($path);

        foreach ($generations as $item) {
            DB::table('generations')->insert([
                'name' => $item->name,
                'slug' => $item->slug,
                'description' => $item->description,
                'image' => $item->image,
            ]);
        }
    }
}
