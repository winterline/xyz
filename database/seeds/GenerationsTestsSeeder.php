<?php

use Illuminate\Database\Seeder;

class GenerationsTestsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = File::get('database/data/generations_tests.json');

        $tests = json_decode($path);

        foreach ($tests as $item) {
            DB::table('generation_test')->insert([
                'generation_id' => $item->generation_id,
                'test_id' => $item->test_id
            ]);
            DB::table('generation_olympiad')->insert([
                'generation_id' => $item->generation_id,
                'olympiad_id' => $item->test_id
            ]);
            DB::table('generation_master')->insert([
                'generation_id' => $item->generation_id,
                'master_id' => $item->test_id
            ]);
            DB::table('generation_project')->insert([
                'generation_id' => $item->generation_id,
                'project_id' => $item->test_id
            ]);
            DB::table('generation_link')->insert([
                'generation_id' => $item->generation_id,
                'link_id' => $item->test_id
            ]);
        }
    }
}
