<?php

use Illuminate\Database\Seeder;

class LinksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = File::get('database/data/links.json');

        $links = json_decode($path);

        foreach ($links as $item) {
            DB::table('links')->insert([
                'title' => $item->title,
                'link' => $item->link,
                'description' => $item->description,
                'image' => $item->image,
            ]);
        }
    }
}
