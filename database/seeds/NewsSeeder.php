<?php

use Illuminate\Database\Seeder;

class NewsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = File::get('database/data/news.json');

        $news = json_decode($path);

        foreach ($news as $item) {
            DB::table('news')->insert([
                'title' => $item->title,
                'description' => $item->description,
                'image' => $item->image,
            ]);
        }
    }
}
