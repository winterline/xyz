<?php

use Illuminate\Database\Seeder;

class ServicesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = File::get('database/data/services.json');

        $services = json_decode($path);

        foreach ($services as $item) {
            DB::table('services')->insert([
                'title' => $item->title,
                'description' => $item->description,
                'image' => $item->image,
            ]);
        }
    }
}
