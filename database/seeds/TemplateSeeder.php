<?php

use Illuminate\Database\Seeder;

class TemplateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = File::get('database/data/template.json');

        $templates = json_decode($path);

        foreach ($templates as $item) {
            DB::table('olympiads')->insert([
                'title' => $item->title,
                'sub_title' => $item->title,
                'description' => $item->description,
                'image' => $item->image,
            ]);
            DB::table('masters')->insert([
                'title' => $item->title,
                'sub_title' => $item->title,
                'description' => $item->description,
                'image' => $item->image,
            ]);
            DB::table('projects')->insert([
                'title' => $item->title,
                'sub_title' => $item->title,
                'description' => $item->description,
                'image' => $item->image,
            ]);
        }
    }
}
