<?php

use Illuminate\Database\Seeder;

class TestsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = File::get('database/data/tests.json');

        $test = json_decode($path);

        foreach ($test as $item) {
            DB::table('tests')->insert([
                'title' => $item->title,
                'sub_title' => $item->title,
                'description' => $item->description,
                'file' => $item->file,
                'pre_file' => $item->file,
            ]);
        }
    }
}
