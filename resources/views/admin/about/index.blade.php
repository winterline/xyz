@extends('layouts.admin')

@section('content')
<main class="content-wrapper">
    <div class="" id="">
        <div class="container-fluid">
            <h1>О нас</h1>
            <button type="button" class="btn btn-primary" data-toggle="collapse" href="#edit" aria-expanded="false" aria-controls="edit">Редактировать</button>
            <div class="row" style="margin-top: 5em;">
                <div class="col-4">
                    <img src="{{asset('image/' . $about->image)}}" alt="{{$about->image}}" class="w-100">
                </div>
                <div class="col-8">
                    <p>{!!$about->description!!}</p>
                </div>
            </div>
            <div class="row" style="margin-top: 2em">
                <div class="collapse col" id="edit">
                    <h2>
                        Редактирование
                    </h2>
                    <form action="{{route('update_about', $about)}}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="description">Текст </label>
                            <textarea class="form-control" id="description" name="description" rows="6">{{$about->description}}
                            </textarea>
                        </div>
                        <div class="form-group">
                            <label for="image">Картинка</label>
                            <input type="file" class="form-control-file" name="image" id="image">
                        </div>

                        <button type="submit" class="btn btn-primary">Отредактирвоать</button>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        setTimeout(function(){
            var editor = CKEDITOR.replace( 'description' );
        },100);
    </script>
</main>


@endsection
