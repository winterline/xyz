@extends('layouts.admin')

@section('content')
<main class="content-wrapper">
    <div class="" id="">
        <div class="container-fluid">
            <h1>Контакты</h1>
            <button type="button" class="btn btn-primary" data-toggle="collapse" href="#edit" aria-expanded="false" aria-controls="edit">Редактировать</button>
            <div class="row" style="margin-top: 2em;">
                <div class="col">
                    <p>Телефон: {{$contacts->phone}}</p>
                    <p>Email: {{$contacts->email}}</p>
                    <p>Адресс: {{$contacts->address}}</p>
                    <p>Время работы: {{$contacts->time_work}}</p>
                </div>
            </div>
            <div class="row" style="margin-top: 2em">
                <div class="collapse col" id="edit">
                    <h2>
                        Редактирование
                    </h2>
                    <form action="{{route('update_contacts', $contacts)}}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="phone">Телефон</label>
                            <input type="text" class="form-control" id="phone" name="phone" value="{{$contacts->phone}}">
                        </div>

                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="text" class="form-control" id="email" name="email" value="{{$contacts->email}}">
                        </div>

                        <div class="form-group">
                            <label for="address">Адресс</label>
                            <input type="text" class="form-control" id="address" name="address" value="{{$contacts->address}}">
                        </div>

                        <div class="form-group">
                            <label for="time_work">Время работы</label>
                            <input type="text" class="form-control" id="time_work" name="time_work" value="{{$contacts->time_work}}">
                        </div>

                        <button type="submit" class="btn btn-primary">Отредактирвоать</button>

                    </form>
                </div>
            </div>
        </div>
    </div>
</main>


@endsection
