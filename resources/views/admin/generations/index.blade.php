@extends('layouts.admin')

@section('content')

<main class="content-wrapper">
    <div class="" id="">

        <div class="container-fluid">
            <h1>{{ $generation->name }}</h1>

            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active text-dark" id="pills-home-tab" data-toggle="pill" href="#pills-main" role="tab" aria-controls="pills-home" aria-selected="true">Основное</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-dark" id="pills-profile-tab" data-toggle="pill" href="#pills-tests" role="tab" aria-controls="pills-profile" aria-selected="false">Тесты</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-dark" id="pills-contact-tab" data-toggle="pill" href="#pills-olimp" role="tab" aria-controls="pills-contact" aria-selected="false">Олимпиады\конкуры</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-dark" id="pills-contact-tab" data-toggle="pill" href="#pills-master" role="tab" aria-controls="pills-contact" aria-selected="false">Мастер-классы</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-dark" id="pills-contact-tab" data-toggle="pill" href="#pills-progects" role="tab" aria-controls="pills-contact" aria-selected="false">Проектная деятельность</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-dark" id="pills-contact-tab" data-toggle="pill" href="#pills-links" role="tab" aria-controls="pills-contact" aria-selected="false">Полезные ресурсы</a>
                </li>
            </ul>
            <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-main" role="tabpanel" aria-labelledby="pills-home-tab">
                    <h1>
                        Основное
                    </h1>

                    <p>Заголовок поколения: {{$generation->name}}</p>
                    <p>Описание: {{$generation->description}}</p>
                    Изображение поколени:
                    <br>
                    <img src="{{asset('image/' . $generation->image) }}" alt="{{$generation->image}}" width="250">
                    <br>
                    <br>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#edit">
                        Редактировать поколение
                    </button>
                </div>
                <div class="tab-pane fade" id="pills-tests" role="tabpanel" aria-labelledby="pills-profile-tab">
                    <h2>
                        Тесты
                    </h2>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addTest" style="margin-bottom: 1em">
                        Добавить тест
                    </button>
                    <table class="table table-responsive">

                        <thead>
                            <tr>
                                <th scope="col">Картинка</th>
                                <th scope="col">Название</th>
                                <th scope="col">Подзаголовок</th>
                                <th scope="col">Файл</th>
                                <th scope="col">Демо Файл</th>
                                <th scope="col">Описание</th>
                                <th scope="col">Действия</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($tests as $item)
                            <tr>
                                <th><img src="{{asset('image/' . $item->image) }}" alt="{{$item->image}}" width="250"></th>
                                <th>{{$item->title}}</th>
                                <th>{{$item->sub_title}}</th>
                                <th>{{ mb_strimwidth($item->file, 0, 20, "...")}}</th>
                                <th>{{ mb_strimwidth($item->pre_file, 0, 20, "...")}}</th>
                                <th>{{ mb_strimwidth($item->description, 0, 20, "...")}}</th>
                                <th>
                                    <span>
                                        <a href="{{ route('edit_test', $item->id)}}">
                                            <img src="https://img.icons8.com/carbon-copy/34/000000/pencil.png" />
                                        </a>
                                    </span>
                                    <span onclick="getElementById('destroy-{{$item->id}}').submit()" style="cursor:pointer">
                                        <img src="https://img.icons8.com/android/24/000000/trash.png" />
                                    </span>
                                    <form hidden action="{{ route('delete_test', $item->id)}}" id="destroy-{{$item->id}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                    </form>
                                </th>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
                <div class="tab-pane fade" id="pills-olimp" role="tabpanel" aria-labelledby="pills-contact-tab">
                    <h2>
                        Олипиады \ Конкурсы
                    </h2>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addOlympiad" style="margin-bottom: 1em">
                        Добавить Олипиады \ Конкурсы
                    </button>
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">Картинка</th>
                                <th scope="col">Название</th>
                                <th scope="col">Подзаголовок</th>
                                <th scope="col">Файл</th>
                                <th scope="col">Демо Файл</th>
                                <th scope="col">Описание</th>
                                <th scope="col">Действия</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($olympiads as $item)
                            <tr>
                                <th><img src="{{asset('image/'. $item->image)}}" alt="{{$item->image}}" height="100"></th>
                                <th>{{$item->title}}</th>
                                <th>{{$item->sub_title}}</th>
                                <th>{{ mb_strimwidth($item->file, 0, 20, "...")}}</th>
                                <th>{{ mb_strimwidth($item->pre_file, 0, 20, "...")}}</th>
                                <th>{{ mb_strimwidth($item->description, 0, 20, "...")}}</th>
                                <th>
                                    <span>
                                        <a href="{{ route('edit_olympiad', $item->id)}}">
                                            <img src="https://img.icons8.com/carbon-copy/34/000000/pencil.png" />
                                        </a>
                                    </span>
                                    <span onclick="getElementById('destroy-{{$item->id}}').submit()" style="cursor:pointer">
                                        <img src="https://img.icons8.com/android/24/000000/trash.png" />
                                    </span>
                                    <form hidden action="{{ route('delete_olympiad', $item->id)}}" id="destroy-{{$item->id}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                    </form>
                                </th>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane fade" id="pills-master" role="tabpanel" aria-labelledby="pills-contact-tab">
                    <h2>
                        Мастер-классы
                    </h2>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addMaster" style="margin-bottom: 1em">
                        Добавить Мастер-классы
                    </button>
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">Картинка</th>
                                <th scope="col">Название</th>
                                <th scope="col">Подзаголовок</th>
                                <th scope="col">Файл</th>
                                <th scope="col">Демо Файл</th>
                                <th scope="col">Описание</th>
                                <th scope="col">Действия</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($masters as $item)
                            <tr>
                                <th><img src="{{asset('image/' . $item->image) }}" alt="{{$item->image}}" width="100"></th>
                                <th>{{$item->title}}</th>
                                <th>{{$item->sub_title}}</th>
                                <th>{{ mb_strimwidth($item->file, 0, 20, "...")}}</th>
                                <th>{{ mb_strimwidth($item->pre_file, 0, 20, "...")}}</th>
                                <th>{{ mb_strimwidth($item->description, 0, 20, "...")}}</th>
                                <th>
                                    <span>
                                        <a href="{{ route('edit_master', $item->id)}}">
                                            <img src="https://img.icons8.com/carbon-copy/34/000000/pencil.png" />
                                        </a>
                                    </span>
                                    <span onclick="getElementById('destroy-{{$item->id}}').submit()" style="cursor:pointer">
                                        <img src="https://img.icons8.com/android/24/000000/trash.png" />
                                    </span>
                                    <form hidden action="{{ route('delete_master', $item->id)}}" id="destroy-{{$item->id}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                    </form>
                                </th>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane fade" id="pills-progects" role="tabpanel" aria-labelledby="pills-contact-tab">
                    <h2>
                        Проектная деятельность
                    </h2>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addProject" style="margin-bottom: 1em">
                        Добавить Проектная деятельность
                    </button>
                    <table class="table">

                        <thead>
                            <tr>
                                <th scope="col">Картинка</th>
                                <th scope="col">Название</th>
                                <th scope="col">Подзаголовок</th>
                                <th scope="col">Файл</th>
                                <th scope="col">Демо Файл</th>
                                <th scope="col">Описание</th>
                                <th scope="col">Действия</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($projects as $item)
                            <tr>
                                <th><img src="{{asset('image/' . $item->image) }}" alt="{{$item->image}}" width="100"></th>
                                <th>{{$item->title}}</th>
                                <th>{{$item->sub_title}}</th>
                                <th>{{ mb_strimwidth($item->file, 0, 20, "...")}}</th>
                                <th>{{ mb_strimwidth($item->pre_file, 0, 20, "...")}}</th>
                                <th>{{ mb_strimwidth($item->description, 0, 20, "...")}}</th>
                                <th>
                                    <span>
                                        <a href="{{ route('edit_project', $item->id)}}">
                                            <img src="https://img.icons8.com/carbon-copy/34/000000/pencil.png" />
                                        </a>
                                    </span>
                                    <span onclick="getElementById('destroy-{{$item->id}}').submit()" style="cursor:pointer">
                                        <img src="https://img.icons8.com/android/24/000000/trash.png" />
                                    </span>
                                    <form hidden action="{{ route('delete_prpject', $item->id)}}" id="destroy-{{$item->id}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                    </form>
                                </th>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane fade" id="pills-links" role="tabpanel" aria-labelledby="pills-contact-tab">
                    <h2>
                        Полезные ресурсы
                    </h2>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addLinks" style="margin-bottom: 1em">
                        Добавить Полезные ресурсы
                    </button>
                    <table class="table">

                        <thead>
                            <tr>
                                <th scope="col">Картинка</th>
                                <th scope="col">Название</th>
                                <th scope="col">Описание</th>
                                <th scope="col">Ссылка</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($links as $item)
                            <tr>
                                <th><img src="{{asset('image/' . $item->image) }}" alt="{{$item->image}}" width="100"></th>
                                <th>{{$item->title}}</th>
                                <th>{{ mb_strimwidth($item->description, 0, 20, "...")}}</th>
                                <th>{{ mb_strimwidth($item->link, 0, 20, "...")}}}</th>
                                <th>
                                    <span>
                                        <a href="{{ route('edit_link', $item->id)}}">
                                            <img src="https://img.icons8.com/carbon-copy/34/000000/pencil.png" />
                                        </a>
                                    </span>
                                    <span onclick="getElementById('destroy-{{$item->id}}').submit()" style="cursor:pointer">
                                        <img src="https://img.icons8.com/android/24/000000/trash.png" />
                                    </span>
                                    <form hidden action="{{ route('delete_link', $item->id)}}" id="destroy-{{$item->id}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                    </form>
                                </th>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


        <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Редактировать поколение</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{route('update_generations', $generation->id)}}" method="post" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="name">Название поколения</label>
                                <input type="text" class="form-control" id="name" name="name" value="{{$generation->name}}">
                            </div>
                            <div class="form-group">
                                <label for="description">Описание </label>
                                <textarea class="form-control" id="description" name="description" rows="6">{{$generation->description}}
                                </textarea>
                            </div>
                            <div class="form-group">
                                <label for="image">Картинка</label>
                                <input type="file" class="form-control-file" name="image" id="image">
                            </div>

                            <button type="submit" class="btn btn-primary">Редактировать</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade" id="addTest" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Добавить Тест</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{route('store_test')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="id_generation" value="{{$generation->id}}">
                            <div class="form-group">
                                <label for="title">Название Теста</label>
                                <input type="text" class="form-control" id="title" name="title" value="">
                            </div>
                            <div class="form-group">
                                <label for="title">Краткое описание Теста</label>
                                <input type="text" class="form-control" id="sub_title" name="sub_title" value="">
                            </div>
                            <div class="form-group">
                                <label for="description">Описание </label>
                                <textarea class="form-control" id="description2" name="description" rows="6">
                                </textarea>
                            </div>
                            <div class="form-group">
                                <label for="file">Демо Файл</label>
                                <input type="file" class="form-control-file" name="pre_file" id="pre_file">
                            </div>
                            <div class="form-group">
                                <label for="file">Файл</label>
                                <input type="file" class="form-control-file" name="file" id="file">
                            </div>
                            <div class="form-group">
                                <label for="file">Изображение</label>
                                <input type="file" class="form-control-file" name="image" id="image">
                            </div>

                            <button type="submit" class="btn btn-primary">Добавить тест</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="addOlympiad" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Добавить олимпиаду</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{route('store_olympiad')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="id_generation" value="{{$generation->id}}">

                            <div class="form-group">
                                <label for="title">Название</label>
                                <input type="text" class="form-control" id="title" name="title" value="">
                            </div>

                            <div class="form-group">
                                <label for="sub_title">Краткое описание</label>
                                <input type="text" class="form-control" id="sub_title" name="sub_title" value="">
                            </div>

                            <div class="form-group">
                                <label for="description">Описание </label>
                                <textarea class="form-control" id="description3" name="description" rows="6">
                                </textarea>
                            </div>

                            <div class="form-group">
                                <label for="file">Файл</label>
                                <input type="file" class="form-control-file" name="file" id="file">
                            </div>

                            <div class="form-group">
                                <label for="pre_file">Демо файл</label>
                                <input type="file" class="form-control-file" name="pre_file" id="pre_file">
                            </div>

                            <div class="form-group">
                                <label for="image">Картинка</label>
                                <input type="file" class="form-control-file" name="image" id="image">
                            </div>

                            <button type="submit" class="btn btn-primary">Добавить</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="addMaster" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Добавить Мастер-классы</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{route('store_master')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="id_generation" value="{{$generation->id}}">
                            <div class="form-group">
                                <label for="title">Название</label>
                                <input type="text" class="form-control" id="title" name="title" value="">
                            </div>
                            <div class="form-group">
                                <label for="sub_title">Краткое описание</label>
                                <input type="text" class="form-control" id="sub_title" name="sub_title" value="">
                            </div>
                            <div class="form-group">
                                <label for="description">Описание </label>
                                <textarea class="form-control" id="description4" name="description" rows="6">
                                </textarea>
                            </div>
                            <div class="form-group">
                                <label for="file">Файл</label>
                                <input type="file" class="form-control-file" name="file" id="file">
                            </div>
                            <div class="form-group">
                                <label for="pre_file">Демо файл</label>
                                <input type="file" class="form-control-file" name="pre_file" id="pre_file">
                            </div>
                            <div class="form-group">
                                <label for="image">Картинка</label>
                                <input type="file" class="form-control-file" name="image" id="image">
                            </div>

                            <button type="submit" class="btn btn-primary">Добавить</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="addProject" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Добавить Мастер-классы</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{route('store_project')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="id_generation" value="{{$generation->id}}">
                            <div class="form-group">
                                <label for="title">Название</label>
                                <input type="text" class="form-control" id="title" name="title" value="">
                            </div>
                            <div class="form-group">
                                <label for="sub_title">Краткое описание</label>
                                <input type="text" class="form-control" id="sub_title" name="sub_title" value="">
                            </div>
                            <div class="form-group">
                                <label for="description">Описание </label>
                                <textarea class="form-control" id="description5" name="description" rows="6">
                                </textarea>
                            </div>
                            <div class="form-group">
                                <label for="file">Файл</label>
                                <input type="file" class="form-control-file" name="file" id="file">
                            </div>
                            <div class="form-group">
                                <label for="pre_file">Демо файл</label>
                                <input type="file" class="form-control-file" name="pre_file" id="pre_file">
                            </div>
                            <div class="form-group">
                                <label for="image">Картинка</label>
                                <input type="file" class="form-control-file" name="image" id="image">
                            </div>

                            <button type="submit" class="btn btn-primary">Добавить</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="addLinks" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Добавить Мастер-классы</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{route('store_link')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="id_generation" value="{{$generation->id}}">
                            <div class="form-group">
                                <label for="title">Название</label>
                                <input type="text" class="form-control" id="title" name="title" value="">
                            </div>
                            <div class="form-group">
                                <label for="description">Описание </label>
                                <textarea class="form-control" id="description6" name="description" rows="6">
                                </textarea>
                            </div>
                            <div class="form-group">
                                <label for="link">Ссылка</label>
                                <input type="text" class="form-control" id="link" name="link" value="">
                            </div>
                            <div class="form-group">
                                <label for="image">Картинка</label>
                                <input type="file" class="form-control-file" name="image" id="image">
                            </div>

                            <button type="submit" class="btn btn-primary">Добавить</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <script>
        setTimeout(function(){
            CKEDITOR.replace( 'description' );
            CKEDITOR.replace( 'description2' );
            CKEDITOR.replace( 'description3' );
            CKEDITOR.replace( 'description4' );
            CKEDITOR.replace( 'description5' );
            CKEDITOR.replace( 'description6' );
        },100);
    </script>
</main>


@endsection
