@extends('layouts.admin')

@section('content')
<main class="content-wrapper">
    <div class="" id="">
        <div class="container-fluid">
            <h1>Редактировать мастер-класс</h1>

            <form action="{{route('update_master', $master->id)}}" method="post" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="title">Заголовок</label>
                    <input type="text" class="form-control" id="title" name="title" value="{{$master->title}}">
                </div>

                <div class="form-group">
                    <label for="sub_title">Краткое описание</label>
                    <input type="text" class="form-control" id="sub_title" name="sub_title" value="{{$master->sub_title}}">
                </div>

                <div class="form-group">
                    <label for="description">Описание</label>
                    <textarea class="form-control" id="description" name="description" rows="3">{{$master->description}}</textarea>
                </div>

                <div class="form-group">
                    <label for="file">Файл</label>
                    <input type="file" class="form-control-file" name="file" id="file">
                </div>

                <div class="form-group">
                    <label for="pre_file">Демо файл</label>
                    <input type="file" class="form-control-file" name="pre_file" id="pre_file">
                </div>

                <div class="form-group">
                    <label for="image">Картинка</label>
                    <input type="file" class="form-control-file" name="image" id="image">
                </div>

                <button type="submit" class="btn btn-primary">Редактировать</button>
            </form>

        </div>
    </div>
    <script>
        setTimeout(function(){
            var editor = CKEDITOR.replace( 'description' );
        },100);
    </script>
</main>


@endsection
