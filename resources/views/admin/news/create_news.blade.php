@extends('layouts.admin')

@section('content')
<main class="content-wrapper">
    <div class="" id="">
        <div class="container-fluid">
            <h1>Добавить Новсти</h1>

            <form action="{{route('store_news')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="title">Заголовок новости</label>
                    <input type="text" class="form-control" id="title" name="title" placeholder="Введите заголовок статьи">
                </div>
                {{-- <div class="form-group">
                    <label for="description">Текст новости</label>
                    <textarea class="form-control" id="description" name="description" rows="3"></textarea>
                </div> --}}

                <div class="form-group">
                    <label for="description">Текст новости</label>
                    <textarea class="form-control" id="description" name="description"></textarea>
                </div>
                <div class="form-group">
                    <label for="image">Картинка новости</label>
                    <input type="file" class="form-control-file" name="image" id="image">
                </div>

                <button type="submit" class="btn btn-primary">Добавить</button>
            </form>

        </div>
    </div>
    <script>
        setTimeout(function(){
            var editor = CKEDITOR.replace( 'description' );
        },100);
    </script>
</main>


@endsection
