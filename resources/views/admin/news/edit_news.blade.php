@extends('layouts.admin')

@section('content')
<main class="content-wrapper">
    <div class="" id="">
        <div class="container-fluid">
            <h1>Редактировать Новсти</h1>

            <form action="{{route('update_news', $news->id)}}" method="post" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="title">Заголовок новости</label>
                    <input type="text" class="form-control" id="title" name="title" value="{{$news->title}}">
                </div>
                <div class="form-group">
                    <label for="description">Текст новости</label>
                    <textarea class="form-control" id="description" name="description" rows="3">{{$news->description}}</textarea>
                </div>

                <div class="form-group">
                    <label for="image">Картинка новости</label>
                    <input type="file" class="form-control-file" name="image" id="image">
                </div>

                <button type="submit" class="btn btn-primary">Редактировать</button>
            </form>

        </div>
    </div>
    <script>
        setTimeout(function(){
            var editor = CKEDITOR.replace( 'description' );
        },100);
    </script>
</main>


@endsection
