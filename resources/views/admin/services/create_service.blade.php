@extends('layouts.admin')

@section('content')
<main class="content-wrapper">
    <div class="" id="">
        <div class="container-fluid">
            <h1>Добавить Услугу</h1>

            <form action="{{route('store_service')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="title">Заголовок услуги</label>
                    <input type="text" class="form-control" id="title" name="title" placeholder="Введите заголовок услуги">
                </div>
                <div class="form-group">
                    <label for="description">Текст услуги</label>
                    <textarea class="form-control" id="description" name="description" rows="3"></textarea>
                </div>
                <div class="form-group">
                    <label for="image">Картинка услуги</label>
                    <input type="file" class="form-control-file" name="image" id="image">
                </div>

                <button type="submit" class="btn btn-primary">Добавить</button>
            </form>

        </div>
    </div>
    <script>
        setTimeout(function(){
            var editor = CKEDITOR.replace( 'description' );
        },100);
    </script>
</main>


@endsection
