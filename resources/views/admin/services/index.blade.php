@extends('layouts.admin')

@section('content')
<main class="content-wrapper">
    <div class="" id="">
        <div class="container-fluid">
            <h1>Услуги</h1>
            <a href="{{ route('create_service')}}">
                <button type="button" class="btn btn-primary">Добавить услугу</button>
            </a>
            <br><br>
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">Заголовок</th>
                        <th scope="col">Описание</th>
                        <th scope="col">Картинка</th>
                        <th scope="col">Действия</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($services as $item)
                    <tr>
                        <th>{{$item->title}}</th>
                        <th>{!!$item->description!!}</th>
                        <th>
                            <img src="{{asset('image/' . $item->image) }}" alt="{{$item->name}}" width="200">
                        </th>
                        <th>
                            <span>
                                <a href="{{route('edit_service', $item->id)}}">
                                    <img src="https://img.icons8.com/carbon-copy/34/000000/pencil.png" />
                                </a>
                            </span>
                            <span onclick="getElementById('destroy-{{$item->id}}').submit()" style="cursor:pointer">
                                <img src="https://img.icons8.com/android/24/000000/trash.png" />
                            </span>
                            <form hidden action="{{ route('delete_service', $item->id)}}" id="destroy-{{$item->id}}" method="post">
                                @csrf
                                @method('DELETE')
                            </form>

                        </th>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</main>


@endsection
