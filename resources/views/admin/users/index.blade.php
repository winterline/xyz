@extends('layouts.admin')

@section('content')

<main class="content-wrapper">
    @if (session()->has('message'))
    <div class="alert alert-success" role="alert">
        {{ session()->get('message') }}
    </div>
    @endif
    <div class="container">

        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link text-dark active" id="pills-users-tab" data-toggle="pill" href="#pills-tab" role="tab" aria-controls="pills-tab" aria-selected="true">Пользователи</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-dark" id="pills-pay-tab" data-toggle="pill" href="#pills-pay" role="tab" aria-controls="pills-pay" aria-selected="false">Оплата</a>
            </li>
        </ul>
    </div>
    <div class="tab-content" id="pills-tabContent">
        <div class="tab-pane fade show active" id="pills-tab" role="tabpanel" aria-labelledby="pills-users-tab">
            <div class="container-fluid">
                <h1>Пользователи</h1>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">ФИО</th>
                            <th scope="col">Email</th>
                            <th scope="col">Роль</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $item)
                        <tr>
                            <th>{{$item->name}}</th>
                            <th>{{$item->email}}</th>
                            <th>
                                @if ($item->role == 0)
                                Пользователь
                                @else
                                Администратор
                                @endif
                            </th>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
        <div class="tab-pane fade" id="pills-pay" role="tabpanel" aria-labelledby="pills-pay-tab">
            <div class="container-fluid">
                <h1>Пользователи оплатившие курсы</h1>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Номер транзаакции</th>
                            <th scope="col">Пользователь оплативший контент</th>
                            <th scope="col">Название контента</th>
                            <th scope="col">Дата</th>
                            <th scope="col">Действие</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($usersPay as $item)
                        <tr>
                            <th>{{$item->id}}</th>
                            <th> {{$item->user_name}}
                                <br> {{$item->user_email}}
                            </th>
                            <th>{{ mb_strimwidth($item->name_file, 0, 20, "...")}}}</th>
                            <th>{{date('d.m.Y', strtotime($item->updated_at))}}</th>
                            <th>
                                <button class="btn btn-primary" onclick="document.getElementById('sendFile-{{$item->id}}').submit();">Отправить контест</button>
                                <form id="sendFile-{{$item->id}}" name="sendFile-{{$item->id}}" action="{{ route('sendFile') }}" method="POST" style="display: none;">
                                    @csrf
                                    <input type="text" name="id" value="{{$item->id}}">
                                </form>
                            </th>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>

    </div>
</main>


@endsection
