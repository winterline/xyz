<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>

<body>
    <h1>Обратная связь</h1>
    <div>
        Имя отправителя: {{$data['name']}}
        <br>
        Email отправителя: {{$data['email']}}
        <br>
        Сообщение: {{$data['message']}}
    </div>
</body>

</html>
