@extends('layouts.main')

@section('content')

<main class="content-wrapper">
    <div class="container-fluid" style="margin-top: 3em">
        <h1 class="text-center main-title"> {{$generation->name}} </h1>
    </div>
    <div class="container-fluid">
        <div class="container-fluid" style="margin-top: 2em">
            <div class="container-fluid">
                <h2 class="text-left" style="color: #01ac55">Профориентационные тесты: </h2>
            </div>
            <div id="carousel1" class="myCarousel">
                <div class="gallery">
                    <ul class="images">
                        <div class="items-myslider">
                            @foreach ($tests as $item)
                            <li>
                                <div>
                                    <a href="{{route('example_view',[$generation->slug, 'tests', $item->id] )}}">
                                        @if ($item->image != 'defoalt.jpg' && $item->image != '')
                                        <img src="{{asset('image/' . $item->image) }}" alt="" height="255" width="400">
                                        @else
                                        <img src="{{asset('image/default.jpg')}}" alt="">
                                        @endif
                                        <h4 class="text-dark" style="font-size: 14px; margin-top: 5px">
                                            Дата: {{$item->updated_at}} 11.12.2020
                                        </h4>
                                        <h4 style=" color: skyblue">
                                            {{$item->title}}
                                        </h4>
                                    </a>
                                </div>
                            </li>
                            @endforeach
                        </div>
                    </ul>
                </div>
                @if( sizeof($tests) > 3)
                <div class="d-flex justify-content-center">
                    <button class="arrow next" style="background: none;border: none">
                        <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="24" height="24" viewBox="0 0 172 172" style=" fill:#000000;">
                            <g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal">
                                <path d="M0,172v-172h172v172z" fill="none"></path>
                                <g fill="#000000">
                                    <path d="M71.66667,35.32943l-50.67057,50.67057l50.67057,50.67057l10.75,-10.75l-32.75391,-32.7539h100.83724v-14.33333h-100.83724l32.75391,-32.75391z"></path>
                                </g>
                            </g>
                        </svg>
                    </button>
                    <button class="arrow prev" style="background: none;border: none"><img src="https://img.icons8.com/material-outlined/24/000000/right.png" /></button>
                </div>
                @endif
            </div>
        </div>
    </div>
    <script>
        /* конфигурация */
        let width = 420; // ширина картинки
        let count = 3; // видимое количество изображений
        console.log('carousel1:', carousel1)
        let list = carousel1.querySelector('ul');
        let listElems = carousel1.querySelectorAll('li');

        let position = 0; // положение ленты прокрутки

        carousel1.querySelector('.prev').onclick = function() {
          // сдвиг влево/парво
          if(position > (listElems.length - count) * width * -1) {
            position -= width * count;

                list.style.marginLeft = position + 'px';
            }

        };

        carousel1.querySelector('.next').onclick = function() {
          // сдвиг вправо
          if (position < 0) {
            position += width * count;
              list.style.marginLeft = position + 'px';
          }


        };
    </script>

    <div class="container-fluid" style="margin-top: 2em">
        <div class="container-fluid">
            <div class="container-fluid">
                <h2 class="text-left" style="color: #01ac55">Олимпиады \ конкурсы: </h2>
            </div>

            <div id="carousel2" class="myCarousel">
                <div class="gallery">
                    <ul class="images">
                        <div class="items-myslider">
                            @foreach ($olympiads as $item)
                            <li>
                                <div>
                                    <a href="{{route('example_view',[$generation->slug, 'olympiads', $item->id] )}}">
                                        @if ($item->image != 'defoalt.jpg' && $item->image != '' && $item->image != 'default.jpg')
                                        <img src="{{asset('image/' . $item->image) }}" alt="" height="255" width="400">
                                        @else
                                        <img src="{{asset('image/default.jpg')}}" alt="">
                                        @endif
                                        <h4 class="text-dark" style="font-size: 14px; margin-top: 5px">
                                            Дата: {{$item->updated_at}} 11.12.2020
                                        </h4>
                                        <h4 style=" color: skyblue">
                                            {{$item->title}}
                                        </h4>
                                    </a>
                                </div>
                            </li>
                            @endforeach
                        </div>
                    </ul>
                </div>
                @if( sizeof($olympiads) > 3)
                <div class="d-flex justify-content-center">
                    <button class="arrow next" style="background: none;border: none">
                        <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="24" height="24" viewBox="0 0 172 172" style=" fill:#000000;">
                            <g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal">
                                <path d="M0,172v-172h172v172z" fill="none"></path>
                                <g fill="#000000">
                                    <path d="M71.66667,35.32943l-50.67057,50.67057l50.67057,50.67057l10.75,-10.75l-32.75391,-32.7539h100.83724v-14.33333h-100.83724l32.75391,-32.75391z"></path>
                                </g>
                            </g>
                        </svg>
                    </button>
                    <button class="arrow prev" style="background: none;border: none"><img src="https://img.icons8.com/material-outlined/24/000000/right.png" /></button>
                </div>
                @endif
            </div>
        </div>
    </div>
    <script>
        console.log('test');
        /* конфигурация */
        let width2 = 420; // ширина картинки
        let count2 = 3; // видимое количество изображений
        console.log('carousel2:', carousel2);

        let list2 = carousel2.querySelector('ul');
        let listElems2 = carousel2.querySelectorAll('li');

        let position2 = 0; // положение ленты прокрутки

        carousel2.querySelector('.prev').onclick = function() {
          // сдвиг влево/парво
          if(position2 > (listElems2.length - count2) * width2 * -1) {
            position2 -= width2 * count2;

                list2.style.marginLeft = position2 + 'px';
            }

        };

        carousel2.querySelector('.next').onclick = function() {
          // сдвиг вправо
          console.log('1. next ', position)
          console.log('2. next ', position)
          if (position2 < 0) {
            position2 += width2 * count2;
              list2.style.marginLeft = position2 + 'px';
          }


        };
    </script>

    <div class="container-fluid" style="margin-top: 2em">
        <div class="container-fluid">
            <div class="container-fluid">
                <h2 class="text-left" style="color: #01ac55">Мастер-классы: </h2>
            </div>
            <div id="carousel3" class="myCarousel">
                <div class="gallery">
                    <ul class="images">
                        <div class="items-myslider">
                            @foreach ($masters as $item)
                            <li>
                                <div>
                                    <a href="{{route('example_view',[$generation->slug, 'masters', $item->id] )}}">
                                        @if ($item->image != 'defoalt.jpg' && $item->image != '' && $item->image != 'default.jpg')
                                        <img src="{{asset('image/' . $item->image) }}" alt="" height="255" width="400">
                                        @else
                                        <img src="{{asset('image/default.jpg')}}" alt="">
                                        @endif
                                        <h4 class="text-dark" style="font-size: 14px; margin-top: 5px">
                                            Дата: {{$item->updated_at}} 11.12.2020
                                        </h4>
                                        <h4 style=" color: skyblue">
                                            {{$item->title}}
                                        </h4>
                                    </a>
                                </div>
                            </li>
                            @endforeach
                        </div>
                    </ul>
                </div>
                @if( sizeof($masters) > 3)

                <div class="d-flex justify-content-center">
                    <button class="arrow next" style="background: none;border: none">
                        <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="24" height="24" viewBox="0 0 172 172" style=" fill:#000000;">
                            <g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal">
                                <path d="M0,172v-172h172v172z" fill="none"></path>
                                <g fill="#000000">
                                    <path d="M71.66667,35.32943l-50.67057,50.67057l50.67057,50.67057l10.75,-10.75l-32.75391,-32.7539h100.83724v-14.33333h-100.83724l32.75391,-32.75391z"></path>
                                </g>
                            </g>
                        </svg>
                    </button>
                    <button class="arrow prev" style="background: none;border: none"><img src="https://img.icons8.com/material-outlined/24/000000/right.png" /></button>
                </div>
                @endif
            </div>
        </div>
    </div>
    <script>
        console.log('test');
        /* конфигурация */
        let width3 = 420; // ширина картинки
        let count3 = 3; // видимое количество изображений
        console.log('carousel3:', carousel3);

        let list3 = carousel3.querySelector('ul');
        let listElems3 = carousel3.querySelectorAll('li');

        let position3 = 0; // положение ленты прокрутки

        carousel3.querySelector('.prev').onclick = function() {
          // сдвиг влево/парво
          if(position3 > (listElems3.length - count3) * width3 * -1) {
            position3 -= width3 * count3;

                list3.style.marginLeft = position3 + 'px';
            }

        };

        carousel3.querySelector('.next').onclick = function() {
          // сдвиг вправо
          console.log('1. next ', position)
          console.log('2. next ', position)
          if (position3 < 0) {
            position3 += width3 * count3;
              list3.style.marginLeft = position3 + 'px';
          }


        };
    </script>



    <div class="container-fluid" style="margin-top: 2em">
        <div class="container-fluid">
            <h2 class="text-left" style="color: #01ac55">Проектная деятельность: </h2>
        </div>
        <div class="container-fluid">
            <div id="carousel4" class="myCarousel">
                <div class="gallery">
                    <ul class="images">
                        <div class="items-myslider">
                            @foreach ($projects as $item)
                            <li>
                                <div>
                                    <a href="{{route('example_view',[$generation->slug, 'projects', $item->id] )}}">
                                        @if ($item->image != 'defoalt.jpg' && $item->image != '' && $item->image != 'default.jpg')
                                        <img src="{{asset('image/' . $item->image) }}" alt="" height="255" width="400">
                                        @else
                                        <img src="{{asset('image/default.jpg')}}" alt="">
                                        @endif
                                        <h4 class="text-dark" style="font-size: 14px; margin-top: 5px">
                                            Дата: {{$item->updated_at}} 11.12.2020
                                        </h4>
                                        <h4 style=" color: skyblue">
                                            {{$item->title}}
                                        </h4>
                                    </a>
                                </div>
                            </li>
                            @endforeach
                        </div>
                    </ul>
                </div>
                @if( sizeof($projects) > 3)

                <div class="d-flex justify-content-center">
                    <button class="arrow next" style="background: none;border: none">
                        <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="24" height="24" viewBox="0 0 172 172" style=" fill:#000000;">
                            <g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal">
                                <path d="M0,172v-172h172v172z" fill="none"></path>
                                <g fill="#000000">
                                    <path d="M71.66667,35.32943l-50.67057,50.67057l50.67057,50.67057l10.75,-10.75l-32.75391,-32.7539h100.83724v-14.33333h-100.83724l32.75391,-32.75391z"></path>
                                </g>
                            </g>
                        </svg>
                    </button>
                    <button class="arrow prev" style="background: none;border: none"><img src="https://img.icons8.com/material-outlined/24/000000/right.png" /></button>
                </div>
                @endif
            </div>
        </div>
    </div>
    <script>
        console.log('test');
        /* конфигурация */
        let width4 = 420; // ширина картинки
        let count4 = 3; // видимое количество изображений
        console.log('carousel4:', carousel4);

        let list4 = carousel4.querySelector('ul');
        let listElems4 = carousel4.querySelectorAll('li');

        let position4 = 0; // положение ленты прокрутки

        carousel4.querySelector('.prev').onclick = function() {
          // сдвиг влево/парво
          if(position4 > (listElems4.length - count4) * width4 * -1) {
            position4 -= width4 * count4;

                list4.style.marginLeft = position4 + 'px';
            }

        };

        carousel4.querySelector('.next').onclick = function() {
          // сдвиг вправо
          console.log('1. next ', position)
          console.log('2. next ', position)
          if (position4 < 0) {
            position4 += width4 * count4;
              list4.style.marginLeft = position4 + 'px';
          }


        };
    </script>

    <div class="container-fluid" style="margin-top: 2em">
        <h1 class="text-center main-title" style="color: #01ac55">Полезные ресурсы: </h1>
        <div class="container-fluid">

            <div class="row">
                <div class="row justify-content-around">
                    @foreach ($links as $item)
                    <div class="card {{$loop->index == 0 || $loop->index % 3 == 0  ? 'col-5' : 'col-3'}}" style="margin: 10px; padding: 0">
                        <div class="d-flex align-items-between flex-column">
                            @if ($item->image != '' && $item->image != 'default.jpg' && $item->image != 'defoalt.jpg')
                            <a href="{{$item->link}}">
                                <img src="{{asset('image/' . $item->image) }}" alt="" class="card-img-top w-100">
                            </a>
                            @else
                            <a href="{{$item->link}}">
                                <img src="{{asset('image/default.jpg')}}" alt="" class="card-img-top w-100">
                            </a>
                            @endif
                            <div class="card-body text-dark">
                                <h5 class="card-title" style=" color: skyblue"> {{$item->title}}</h5>
                                <p class="card-text">{!!$item->description!!}</p>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</main>


@endsection
