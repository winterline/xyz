@extends('layouts.app')

@section('content')
<div class="container" style="background: #eee; padding: 25px; border-radius: 10px">
    <div class="container">
        <h2>Данные пользователя:</h2>
    </div>
    <div class="container">
        @if(Auth::user()->role == 10)
        <a href="{{url('/admin/index')}}" class="btn btn-primary">Панель администратора</a>
        <br>
        <br>
        @endif
        <ul class="list-group list-group-flush">
            <li class="list-group-item">ФИО: {{ Auth::user()->name }}</li>
            <li class="list-group-item">Email: {{ Auth::user()->email }}</li>
            <li class="list-group-item">Телефон: {{ Auth::user()->phone }}</li>
            <li class="list-group-item">Дата рождения: {{date('d.m.Y', strtotime(Auth::user()->age))}}</li>
        </ul>

    </div>
</div>
<div id="contacts" class="navbar-fixed-bottom row-fluid footer" style="background-color: #1f1f1f; margin-top: 3em; color: white">
    <div class="container">
        <div class="d-flex justify-content-start">
            <span class="contacts-title">
                Контакты
            </span>
            <span class="contacts-subtitle">Если у вас остались вопросы по организации и сотрудничеству, мы рады ответить на них</span>
        </div>
    </div>
    <div class="container d-flex justify-content-between">
        <div class="col-6 row">
            <div class="col-6">
                <div style="color: #9e9e9e">Адресс</div>
                <h2>{{$contacts->first()->address}}</h2>
            </div>
            <div class="col-6">
                <div style="color: #9e9e9e">По вопросам сотрудничества</div>
                <h3>{{$contacts->first()->phone}}</h3>
                <div style="color: #9e9e9e">{{$contacts->first()->time_work}}</div>
            </div>
            <div class="col-6">
                <div style="color: #9e9e9e">Электронная почта</div>
                <h3>{{$contacts->first()->email}}</h3>
            </div>
        </div>
        <div class="col-6">
            <form action="{{ route('callback') }}" method="POST">
                @csrf
                <div class="group-inputs justify-content-lg-between">
                    <div class="col-5 my-input-wrap">
                        <input type="text" class="my-input" placeholder="Ваше имя" name="name">
                    </div>
                    <div class="col-5 my-input-wrap">
                        <input type="text" class="my-input" placeholder="Почта" name="email">
                    </div>
                </div>
                <br>
                <div class="col-12 my-input-wrap my-input-wrap-long">
                    <input type="text" placeholder="Ваше сообщение" class="my-input-long" name="message">
                </div>
                <br>
                <div class="col-12" style="padding: 0">
                    <div class="d-flex">
                        <button type="submit" class="my-dark-btn">Отправить</button>
                        <div class="small-info">Нажимая на кнопку "Отправить", вы даете согласие на обработку персональных данных</div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="container white-line">
    </div>
    <div class="container" style="margin-top: 20px">
        <div class="d-flex justify-content-end">
            <div> @2020ИРЦ "ПОРА!"</div>
        </div>
    </div>
</div>
@endsection
