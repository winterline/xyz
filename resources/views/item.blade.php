@extends('layouts.main')

@section('content')

<div class="container">
    @if (session()->has('message'))
    <div class="alert alert-success" role="alert">
        {{ session()->get('message') }}
    </div>
    @endif
    {{-- <img src="{{asset('image/' . $items->image) }}" alt="" width="100%"> --}}
    <div class="jumbotron" style=" background-image: url({{asset('image/' . $items->image) }})!important;">
        <div class="container" style="height: 250px; color: #fff">
            <h1 class="display-4 text-center" style="text-transform: uppercase; font-weight: bold">{{$items->title}}</h1>
            <h3 class="display-4 text-center">{{$items->sub_title}}</h3>
        </div>
    </div>
    <div class="container">
        {!!$items->description!!}
    </div>
    {{-- <p>{{$items}}</p> --}}
    <div class="container">
        <h1>Демо</h1>
        <iframe src="{{asset('file/' . $items->pre_file) }}" width="100%" height="600"></iframe>
    </div>
    @auth
    <div class="container d-flex justify-content-end">
        <div>
            <button type="button" class="my-btn" data-toggle="modal" data-target="#pay">Приобрести тест</button>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="pay" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Оплата</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        Платежные данные:
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">По номер карты: </li>
                            <li class="list-group-item">По номеру телефона: </li>
                            <li class="list-group-item">test</li>
                            <li class="list-group-item">test</li>
                            <li class="list-group-item">test</li>
                        </ul>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        <button class="btn btn-primary" onclick="paySend(event)">Потвердить оплату</button>
                        <form id="payItem" name="payItem" action="{{ route('store_userPay') }}" method="POST" style="display: none;">
                            @csrf
                            <input type="text" name="user_id" value="{{Auth::user()->id}}">
                            <input type="text" name="user_name" value="{{Auth::user()->name}}">
                            <input type="text" name="user_email" value="{{Auth::user()->email}}">
                            <input type="text" name="name_item" value="{{$items->title}}">
                            <input type="text" name="name_file" value="{{$items->file}}">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function paySend(event) {
            console.log('send', event);
            event.preventDefault();
            document.getElementById('payItem').submit();
        }
    </script>
    @endauth
</div>
@endsection
