<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>ПОРА! администрация</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script src="{{ asset('/js/ckeditor/ckeditor.js') }}" type="text/javascript" charset="utf-8"></script>
    <style>
        html {
            position: relative;
            min-height: 100%;
        }

        body {
            padding-top: 4.5rem;
            margin-bottom: 4.5rem;
        }

        .footer {
            position: absolute;
            bottom: 0;
            width: 100%;
            height: 3.5rem;
            line-height: 3.5rem;
            background-color: #ccc;
        }

        .nav-link:hover {
            transition: all 0.4s;
        }

        .nav-link-collapse:after {
            float: right;
            content: '\f067';
            font-family: 'FontAwesome';
        }

        .nav-link-show:after {
            float: right;
            content: '\f068';
            font-family: 'FontAwesome';
        }

        .nav-item ul.nav-second-level {
            padding-left: 0;
        }

        .nav-item ul.nav-second-level>.nav-item {
            padding-left: 20px;
        }

        @media (min-width: 992px) {
            .sidenav {
                position: absolute;
                top: 0;
                left: 0;
                width: 230px;
                height: calc(100vh - 3.5rem);
                margin-top: 3.5rem;
                background: #343a40;
                box-sizing: border-box;
                border-top: 1px solid rgba(0, 0, 0, 0.3);
            }

            .navbar-expand-lg .sidenav {
                flex-direction: column;
            }

            .content-wrapper {
                margin-left: 230px;
            }

            .footer {
                width: calc(100% - 230px);
                margin-left: 230px;
            }
        }
    </style>
</head>

<body>
    <div id="app">
        <nav class="navbar navbar-expand-lg navbar-dark fixed-top bg-dark">
            <a class="navbar-brand" href="{{route('admin-panel')}}">Панель администратора</a>
            <a class="navbar-brand" href="{{route('index')}}">Сайт</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav mr-auto sidenav" id="navAccordion">
                    <li class="nav-item active">
                        <a class="nav-link" href="{{ route('admin-panel')}}">Главная
                            {{-- <span class="sr-only">(current)</span> --}}
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('news') }}">Новсти</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('services') }}">Услуги</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" id="hasSubItems" data-toggle="collapse" data-target="#collapseSubItems2" aria-controls="collapseSubItems2" aria-expanded="false">Поколения</a>
                        <ul class="nav-second-level collapse" id="collapseSubItems2" data-parent="#navAccordion">
                            @foreach ($generations as $item)
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('generations', $item->slug)}}">
                                    <span class="nav-link-text">{{$item->name}}</span>
                                </a>
                            </li>
                            @endforeach


                        </ul>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('contacts')}}">Контакты</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('users')}}">Пользователи</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('about')}}">О нас</a>
                    </li>
                </ul>

            </div>
        </nav>

        @yield('content')

    </div>
    <script>
        $(document).ready(function () {
        $('.nav-link-collapse').on('click', function () {
        $('.nav-link-collapse').not(this).removeClass('nav-link-show');
        $(this).toggleClass('nav-link-show');
            });
        });
    </script>
</body>

</html>
