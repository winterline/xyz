<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Пора</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/mystyle.css') }}" rel="stylesheet">

</head>

<body>
    <div id="app">
        <nav class="navbar navbar-light  justify-content-end" style="height:100px; background: #ffffff;">
            <a href="{{ url('/')}}" class="navbar-brand link-in-header">Новости</a>
            <a href="{{ url('/')}}" class="navbar-brand link-in-header">Услуги</a>
            <a href="{{ url('/')}}" class="navbar-brand link-in-header">О нас</a>
            <a href="{{ url('/')}}" class="navbar-brand link-in-header">Контакты</a>
            <a href="{{ url('/search')}}" class="navbar-brand link-in-header icon-search">
                <img src="https://img.icons8.com/ios-filled/32/000000/search.png" />
            </a>
            @auth
            <a href="{{ url('/home') }}" class="navbar-brand">
                {{ Auth::user()->name }}
                <a class="text-dark" href="{{ route('logout') }}" onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                    Выход
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </a>
            @else
            <a href="{{ route('login') }}" class="navbar-brand">
                <img src="https://img.icons8.com/material/32/000000/name--v1.png" />
            </a>
            @endauth
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>

</body>

</html>
