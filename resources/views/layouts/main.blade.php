<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ПОРА!</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/mystyle.css') }}" rel="stylesheet">

</head>

<body>
    <nav class="navbar navbar-light  justify-content-end" style="height:100px; background: #ffffff;">
        <a href="{{ url('/')}}" class="navbar-brand link-in-header">Новости</a>
        <a href="{{ url('/')}}" class="navbar-brand link-in-header">Услуги</a>
        <a href="{{ url('/')}}" class="navbar-brand link-in-header">О нас</a>
        <a href="{{ url('/')}}" class="navbar-brand link-in-header">Контакты</a>
        <a href="{{ url('/search')}}" class="navbar-brand link-in-header icon-search">
            <img src="https://img.icons8.com/ios-filled/32/000000/search.png" />
        </a>
        @auth
        <a href="{{ url('/home') }}" class="navbar-brand">
            <img src="https://img.icons8.com/material/32/000000/name--v1.png" />
        </a>
        @else
        <a href="{{ route('login') }}" class="navbar-brand">
            <img src="https://img.icons8.com/material/32/000000/name--v1.png" />
        </a>
        @endauth
    </nav>

    @yield('content')

    <div id="contacts" class="navbar-fixed-bottom row-fluid footer" style="background-color: #1f1f1f; margin-top: 3em; color: white">
        <div class="container">
            <div class="d-flex justify-content-start">
                <span class="contacts-title">
                    Контакты
                </span>
                <span class="contacts-subtitle">Если у вас остались вопросы по организации и сотрудничеству, мы рады ответить на них</span>
            </div>
        </div>
        <div class="container d-flex justify-content-between">
            <div class="col-6 row">
                <div class="col-6">
                    <div style="color: #9e9e9e">Адресс</div>
                    <h2>{{$contacts->first()->address}}</h2>
                </div>
                <div class="col-6">
                    <div style="color: #9e9e9e">По вопросам сотрудничества</div>
                    <h3>{{$contacts->first()->phone}}</h3>
                    <div style="color: #9e9e9e">{{$contacts->first()->time_work}}</div>
                </div>
                <div class="col-6">
                    <div style="color: #9e9e9e">Электронная почта</div>
                    <h3>{{$contacts->first()->email}}</h3>
                </div>
            </div>
            <div class="col-6">
                <form action="{{ route('callback') }}" method="POST">
                    @csrf
                    <div class="group-inputs justify-content-lg-between">
                        <div class="col-5 my-input-wrap">
                            <input type="text" class="my-input" placeholder="Ваше имя" name="name">
                        </div>
                        <div class="col-5 my-input-wrap">
                            <input type="text" class="my-input" placeholder="Почта" name="email">
                        </div>
                    </div>
                    <br>
                    <div class="col-12 my-input-wrap my-input-wrap-long">
                        <input type="text" placeholder="Ваше сообщение" class="my-input-long" name="message">
                    </div>
                    <br>
                    <div class="col-12" style="padding: 0">
                        <div class="d-flex">
                            <button type="submit" class="my-dark-btn">Отправить</button>
                            <div class="small-info">Нажимая на кнопку "Отправить", вы даете согласие на обработку персональных данных</div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="container white-line">
        </div>
        <div class="container" style="margin-top: 20px">
            <div class="d-flex justify-content-end">
                <div> @2020ИРЦ "ПОРА!"</div>
            </div>
        </div>
    </div>
</body>

</html>
