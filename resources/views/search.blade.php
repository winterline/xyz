@extends('layouts.main')

@section('content')
<div class="container">
    <div class="search">
        <input class="search-input" type="text" placeholder="Введите текст">
        <br><br>
        <button class="my-btn" onclick="search(event)">Поиск по сайту</button>
        <br><br>
        <div class="text-danger" id="search-info"></div>
    </div>

</div>
<script>
    function search(event) {
        event.preventDefault();
        var error = "<h1>Ничего не найдено</h1>";
        var text = document.getElementById('search-info');
        console.log(error);
        console.log(text);
        text.innerHTML = error;
    }
</script>
@endsection
