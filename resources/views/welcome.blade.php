<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ПОРА!</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/mystyle.css') }}" rel="stylesheet">

    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>

</head>

<body>
    <nav class="navbar navbar-light  justify-content-end" style="height:100px; background: #ffffff;">
        <div id="menu">
            <a href="#news" class="navbar-brand link-in-header">Новости</a>
            <a href="#services" class="navbar-brand link-in-header">Услуги</a>
            <a href="#about_us" class="navbar-brand link-in-header">О нас</a>
            <a href="#contacts" class="navbar-brand link-in-header">Контакты</a>
        </div>
        <a href="{{ url('/search')}}" class="navbar-brand link-in-header icon-search">
            <img src="https://img.icons8.com/ios-filled/32/000000/search.png" />
        </a>
        @auth
        <a href="{{ url('/home') }}" class="navbar-brand">
            <img src="https://img.icons8.com/material/32/000000/name--v1.png" />
        </a>
        @else
        <a href="{{ route('login') }}" class="navbar-brand">
            <img src="https://img.icons8.com/material/32/000000/name--v1.png" />
        </a>
        @endauth
    </nav>

    <div class="container-fluid" style="margin-top: 3em">
        <div class="row  flex-wrap w-100">
            <div class="col-6">

                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">

                    <ol class="carousel-indicators">
                        @foreach ($sliders as $item)

                        <li data-target="#carouselExampleIndicators" data-slide-to="{{ $loop->index }}" class="{{ $loop->first ? 'active' : '' }}"></li>
                        @endforeach


                    </ol>
                    <div class="carousel-inner">
                        @foreach ($sliders as $item)
                        <div class="carousel-item {{ $loop->first ? 'active' : '' }}" style="max-width: 100%; height: auto;">
                            <img class="" src=" {{asset('image/' . $item->image)}}" alt="{{$item->title}}" height="600px">
                            <div class=" carousel-caption d-none d-md-block">
                                <h5 class="text-dark">{{$item->title}}</h5>
                                <button type="button" class="my-btn" data-toggle="modal" data-target="#newsModal-{{$item->id}}">Подробнее</button>
                            </div>
                        </div>
                        @endforeach


                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
            <div class="col-6  w-100">
                <div class="h-100 d-flex align-content-between flex-wrap">
                    @foreach ($generations as $item)
                    @if ($loop->index % 2 == 0)
                    <div class="col-12 generation_link d-flex justify-content-start align-items-center ">
                        <a href="{{route('generations_view', $item->slug)}}">
                            <h3 class="text-generation">
                                {{ $item->name }}
                            </h3>
                        </a>
                    </div>
                    @else
                    <div class="col-12 generation_link d-flex justify-content-end align-items-center">
                        <a href="{{route('generations_view', $item->slug)}}">
                            <h3 class="text-generation ">{{ $item->name }}</h3>
                        </a>
                    </div>
                    @endif
                    @endforeach
                </div>
            </div>
        </div>
        <div class="container-fluid" id="news" style="margin-top: 3em">
            <h1 class="text-center main-title">Новости</h1>
        </div>

        <div id="carousel" class="myCarousel">
            <div class="gallery">
                <ul class="images">
                    <div class="items-myslider">
                        @foreach ($news as $item)
                        <li>
                            <div>
                                @if ($item->image != 'defoalt.jpg')
                                <img src="{{asset('image/' . $item->image) }}" alt="" height="255" width="400">
                                @else
                                <img src="https://dapp.dblog.org/img/default.jpg" alt="">
                                @endif
                                <h4 style="font-size: 14px; margin-top: 5px">
                                    Дата: {{$item->updated_at}} 11.12.2020
                                </h4>
                                <button type="button" data-toggle="modal" data-target="#newsModal-{{$item->id}}" style="background: none; border:none">
                                    <h4 style=" color: skyblue">
                                        {{$item->title}}
                                    </h4>
                                </button>


                            </div>
                        </li>
                        <!-- Modal -->
                        <div class="modal fade" id="newsModal-{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">{{$item->title}}</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body d-flex align-content-between justify-content-center flex-column">
                                        @if ($item->image != 'defoalt.jpg')
                                        <div>
                                            <img src="{{asset('image/' . $item->image) }}" alt="" width="100%" style="margin-bottom: 20px">
                                        </div>
                                        @else
                                        <img src="{{asset('image/default.jpg')}}" alt="">
                                        @endif
                                        <br><br>
                                        <div style="margin-top: 1em">
                                            <span class="lead">
                                                <div>
                                                    {!!$item->description !!}
                                                </div>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </ul>
            </div>
            @if( sizeof($services) > 3)
            <div class="d-flex justify-content-center">
                <button class="arrow next" style="background: none;border: none;  outline:none;">
                    <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="24" height="24" viewBox="0 0 172 172" style=" fill:#000000;">
                        <g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal">
                            <path d="M0,172v-172h172v172z" fill="none"></path>
                            <g fill="#000000">
                                <path d="M71.66667,35.32943l-50.67057,50.67057l50.67057,50.67057l10.75,-10.75l-32.75391,-32.7539h100.83724v-14.33333h-100.83724l32.75391,-32.75391z"></path>
                            </g>
                        </g>
                    </svg>
                </button>
                <button class="arrow prev" style="background: none;border: none; outline:none;"><img src="https://img.icons8.com/material-outlined/24/000000/right.png" /></button>
            </div>
            @endif
        </div>

        <div class="container-fluid" id="services" style="margin-top: 2em">
            <h1 class="text-center main-title">Услуги</h1>
        </div>

        <div id="carousel2" class="myCarousel">
            <div class="gallery">
                <ul class="images">
                    <div class="items-myslider">
                        @foreach ($services as $item)
                        <li>
                            <div>
                                @if ($item->image != 'defoalt.jpg')
                                <img src="{{asset('image/' . $item->image) }}" alt="" height="255" width="400">
                                @else
                                <img src="https://dapp.dblog.org/img/default.jpg" alt="">
                                @endif
                                <h4 style="font-size: 14px; margin-top: 5px">
                                    {{$item->updated_at}}
                                </h4>
                                <button type="button" data-toggle="modal" data-target="#servicesModal-{{$item->id}}" style="background: none; border:none; outline:none;">
                                    <h4 style=" color: skyblue">
                                        {{$item->title}}
                                    </h4>
                                </button>


                            </div>
                        </li>
                        <!-- Modal -->
                        <div class="modal fade" id="servicesModal-{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="exampleModalLabel">{{$item->title}}</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body d-flex align-content-between justify-content-center flex-column">
                                        @if ($item->image != 'defoalt.jpg')
                                        <img src="{{asset('image/' . $item->image) }}" alt="" width="100%" style="margin-bottom: 20px">
                                        @else
                                        <img src="{{asset('image/default.jpg')}}" alt="" style="margin-bottom: 20px">
                                        @endif
                                        <br><br>
                                        <div style="margin-top: 1em">
                                            <span class="lead">
                                                {!!$item->description!!}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </ul>
            </div>
            @if( sizeof($services) > 3)
            <div class="d-flex justify-content-center">
                <button class="arrow next" style="background: none;border: none;outline:none;">
                    <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="24" height="24" viewBox="0 0 172 172" style=" fill:#000000;  outline:none;">
                        <g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal">
                            <path d="M0,172v-172h172v172z" fill="none"></path>
                            <g fill="#000000">
                                <path d="M71.66667,35.32943l-50.67057,50.67057l50.67057,50.67057l10.75,-10.75l-32.75391,-32.7539h100.83724v-14.33333h-100.83724l32.75391,-32.75391z"></path>
                            </g>
                        </g>
                    </svg>
                </button>
                <button class="arrow prev" style="background: none;border: none;  outline:none;"><img src="https://img.icons8.com/material-outlined/24/000000/right.png" /></button>
            </div>
            @endif
        </div>
        <div class="container-fluid" style="margin-top: 3em">
            <h2 class="text-center  main-title" id="about_us">О нас</h2>
            <div class="row justify-content-center align-items-center">
                @foreach ($about as $item)
                <div class="col-6">

                    <img src="{{asset('image/' . $item->image)}}" alt="{{$item->image}}" class="w-100">
                </div>
                <div class="col-6 text-center ">
                    <p class="description">
                        {!!$item->description!!}
                    </p>
                </div>
                @endforeach
            </div>
        </div>
    </div>

    <div id="contacts" class="navbar-fixed-bottom row-fluid footer" style="background-color: #1f1f1f; margin-top: 3em; color: white">
        <div class="container">
            <div class="d-flex justify-content-start">
                <span class="contacts-title">
                    Контакты
                </span>
                <span class="contacts-subtitle">Если у вас остались вопросы по организации и сотрудничеству, мы рады ответить на них</span>
            </div>
        </div>
        <div class="container d-flex justify-content-between">
            <div class="col-6 row">
                <div class="col-6">
                    <div style="color: #9e9e9e">Адресс</div>
                    <h2>{{$contacts->first()->address}}</h2>
                </div>
                <div class="col-6">
                    <div style="color: #9e9e9e">По вопросам сотрудничества</div>
                    <h3>{{$contacts->first()->phone}}</h3>
                    <div style="color: #9e9e9e">{{$contacts->first()->time_work}}</div>
                </div>
                <div class="col-6">
                    <div style="color: #9e9e9e">Электронная почта</div>
                    <h3>{{$contacts->first()->email}}</h3>
                </div>
            </div>
            <div class="col-6">
                <form action="{{ route('callback') }}" method="POST">
                    @csrf
                    <div class="group-inputs justify-content-lg-between">
                        <div class="col-5 my-input-wrap">
                            <input type="text" class="my-input" placeholder="Ваше имя" name="name">
                        </div>
                        <div class="col-5 my-input-wrap">
                            <input type="text" class="my-input" placeholder="Почта" name="email">
                        </div>
                    </div>
                    <br>
                    <div class="col-12 my-input-wrap my-input-wrap-long">
                        <input type="text" placeholder="Ваше сообщение" class="my-input-long" name="message">
                    </div>
                    <br>
                    <div class="col-12" style="padding: 0">
                        <div class="d-flex">
                            <button type="submit" class="my-dark-btn">Отправить</button>
                            <div class="small-info">Нажимая на кнопку "Отправить", вы даете согласие на обработку персональных данных</div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="container white-line">
        </div>
        <div class="container" style="margin-top: 20px">
            <div class="d-flex justify-content-end">
                <div> @2020ИРЦ "ПОРА!"</div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function(){
           $("#menu").on("click","a", function (event) {
               event.preventDefault();
               var id  = $(this).attr('href'),
                   top = $(id).offset().top;
               $('body,html').animate({scrollTop: top}, 1500);
           });
       });
    </script>
    <script>
        /* конфигурация */
        let width = 420; // ширина картинки
        let count = 3; // видимое количество изображений

        console.log('carousel:', carousel)
        let list = carousel.querySelector('ul');
        let listElems = carousel.querySelectorAll('li');

        let position = 0; // положение ленты прокрутки

        carousel.querySelector('.prev').onclick = function() {
          // сдвиг влево/парво
          if(position > (listElems.length - count) * width * -1) {
            position -= width * count;

                list.style.marginLeft = position + 'px';
            }

        };

        carousel.querySelector('.next').onclick = function() {
          // сдвиг вправо
          console.log('1. next ', position)
          console.log('2. next ', position)
          if (position < 0) {
            position += width * count;
              list.style.marginLeft = position + 'px';
          }


        };
    </script>
    <script>
        /* конфигурация */
            let width2 = 420; // ширина картинки
            let count2 = 3; // видимое количество изображений

            console.log('carousel2:', carousel2)
            let list2 = carousel2.querySelector('ul');
            let listElems2 = carousel2.querySelectorAll('li');

            let position2 = 0; // положение ленты прокрутки

            carousel2.querySelector('.prev').onclick = function() {
                console.log('position2', position2)
              // сдвиг влево/парво
              if(position2 > (listElems2.length - count2) * width2 * -1) {
                position2 -= width2 * count2;

                    list2.style.marginLeft = position2 + 'px';
                }

            };

            carousel2.querySelector('.next').onclick = function() {
              // сдвиг вправо
              if (position2 < 0) {
                console.log('position2', position2)

                position2 += width2 * count2;
                  list2.style.marginLeft = position2 + 'px';
              }
            };
    </script>
</body>

</html>
