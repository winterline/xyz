<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MainController@index')->name('index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/search', 'MainController@search')->name('search');
Route::get('generations/{slug}', 'MainController@generations')->name('generations_view');
Route::get('generations/{slug}/{item}/{id}', 'MainController@example')->name('example_view');
Route::post('store_userPay', 'UserPayController@store')->name('store_userPay');
Route::post('sendFileContents', 'SendEmailController@sendFileContest')->name('sendFile');
Route::post('callback', 'SendEmailController@callback')->name('callback');



Route::group(
    [
        'prefix' => 'admin',
        'middleware' => ['auth'],
    ],
    function () {
        Route::get('index', 'AdminController@admin')->name('admin-panel');

        // NEWS
        Route::get('news', 'NewsController@news')->name('news');
        Route::get('create_news', 'NewsController@create_news')->name('create_news');
        Route::post('store_news', 'NewsController@store_news')->name('store_news');
        Route::get('edit_news/{id}', 'NewsController@edit_news')->name('edit_news');
        Route::put('update_news/{id}', 'NewsController@update_news')->name('update_news');
        Route::delete('delete_news/{id}', 'NewsController@delete_news')->name('delete_news');

        // SERVICES
        Route::get('services', 'ServiceController@services')->name('services');
        Route::get('create_service', 'ServiceController@create_service')->name('create_service');
        Route::post('store_service', 'ServiceController@store_service')->name('store_service');
        Route::get('edit_service/{id}', 'ServiceController@edit_service')->name('edit_service');
        Route::put('update_service/{id}', 'ServiceController@update_service')->name('update_service');
        Route::delete('delete_service/{id}', 'ServiceController@delete_service')->name('delete_service');

        // ABOUT
        Route::get('about', 'AboutController@about')->name('about');
        Route::put('update_about', 'AboutController@update_about')->name('update_about');

        // CONTACTS
        Route::get('contacts', 'AdminController@contacts')->name('contacts');
        Route::put('update_contacts', 'AdminController@update_contacts')->name('update_contacts');

        // USERS
        Route::get('users', 'AdminController@users')->name('users');

        // GENERATIONS
        Route::get('generations/{slug}', 'AdminController@generations')->name('generations');
        Route::put('update_generations/{id}', 'AdminController@update_generations')->name('update_generations');

        // TESTS
        Route::post('store_test', 'TestController@store_test')->name('store_test');
        Route::get('edit_test/{id}', 'TestController@edit_test')->name('edit_test');
        Route::put('update_test/{id}', 'TestController@update_test')->name('update_test');
        Route::delete('delete_test/{id}', 'TestController@delete_test')->name('delete_test');

        // OLYMPIADS
        Route::post('store_olympiad', 'OlympiadController@store_olympiad')->name('store_olympiad');
        Route::get('edit_olympiad/{id}', 'OlympiadController@edit_olympiad')->name('edit_olympiad');
        Route::put('update_olympiad/{id}', 'OlympiadController@update_olympiad')->name('update_olympiad');
        Route::delete('delete_olympiad/{id}', 'OlympiadController@delete_olympiad')->name('delete_olympiad');

        // MASTERS
        Route::post('store_master', 'MasterController@store_master')->name('store_master');
        Route::get('edit_master/{id}', 'MasterController@edit_master')->name('edit_master');
        Route::put('update_master/{id}', 'MasterController@update_master')->name('update_master');
        Route::delete('delete_master/{id}', 'MasterController@delete_master')->name('delete_master');

        // PROGECTS
        Route::post('store_project', 'ProjectController@store_project')->name('store_project');
        Route::get('edit_project/{id}', 'ProjectController@edit_project')->name('edit_project');
        Route::put('update_project/{id}', 'ProjectController@update_project')->name('update_project');
        Route::delete('delete_prpject/{id}', 'ProjectController@delete_prpject')->name('delete_prpject');

        // LINKS
        Route::post('store_link', 'LinkController@store_link')->name('store_link');
        Route::get('edit_link/{id}', 'LinkController@edit_link')->name('edit_link');
        Route::put('update_link/{id}', 'LinkController@update_link')->name('update_link');
        Route::delete('delete_link/{id}', 'LinkController@delete_link')->name('delete_link');
    }
);
